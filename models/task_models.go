package models

import (
	"be-crud/db"
	"net/http"
)

type Barang struct {
	Id        int    `json:"id"`
	Nama      string `json:"nama"`
	Deskripsi string `json:"deskripsi"`
}

func DataTask() (Response, error) {
	var obj Barang
	var arrobj []Barang
	var res Response

	con := db.CreateCon()

	sqlStatement := "SELECT  * FROM tb_task"
	rows, err := con.Query(sqlStatement)

	defer rows.Close()

	if err != nil {
		return res, err
	}

	for rows.Next() {
		err = rows.Scan(&obj.Id, &obj.Nama, &obj.Deskripsi)

		if err != nil {
			return res, err
		}
		arrobj = append(arrobj, obj)
	}

	res.Status = http.StatusOK
	res.Message = "Sukses"
	res.Data = arrobj

	return res, nil

}

func SimpanData(nama string, deskripsi string) (Response, error) {

	var res Response
	con := db.CreateCon()

	sqlStatement := "INSERT INTO tb_task (nama,deskripsi) VALUES (? , ?)"

	stmt, err := con.Prepare(sqlStatement)
	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(nama, deskripsi)

	if err != nil {
		return res, err
	}

	getIdLast, err := result.LastInsertId()

	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Sukses Tambah Data"
	res.Data = map[string]int64{
		"getIdLast": getIdLast,
	}
	return res, err
}

func UpdateData(id int, nama string, deskripsi string) (Response, error) {
	var res Response

	con := db.CreateCon()

	sqlStatement := "UPDATE tb_task SET nama=?, deskripsi=? WHERE id =?"

	stmt, err := con.Prepare(sqlStatement)
	if err != nil {
		return res, err
	}
	result, err := stmt.Exec(nama, deskripsi, id)

	if err != nil {
		return res, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Sukses Update Data"
	res.Data = map[string]int64{
		"rows": rowsAffected,
	}
	return res, nil

}

func DeletData(id int) (Response, error) {
	var res Response

	con := db.CreateCon()

	sqlStatement := "DELETE FROM tb_task WHERE id =?"

	stmt, err := con.Prepare(sqlStatement)
	if err != nil {
		return res, err
	}
	result, err := stmt.Exec(id)

	if err != nil {
		return res, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Sukses Delete Data"
	res.Data = map[string]int64{
		"rows": rowsAffected,
	}
	return res, nil

}
