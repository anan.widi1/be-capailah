package main

import (
	"be-crud/db"
	"be-crud/routes"
)

func main() {
	db.Init()
	e := routes.Init()
	e.Logger.Fatal(e.Start(":1204"))
}
