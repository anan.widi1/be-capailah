package controllers

import (
	"be-crud/models"
	"github.com/labstack/echo/v4"
	"net/http"
	"strconv"
)

func DataTask(c echo.Context) error {
	result, err := models.DataTask()

	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	return c.JSON(http.StatusOK, result)
}

func SimpanController(c echo.Context) error {
	nama := c.FormValue("nama")
	deskripsi := c.FormValue("deskripsi")

	result, err := models.SimpanData(nama, deskripsi)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	return c.JSON(http.StatusOK, result)

}

func UpdateController(c echo.Context) error {
	id := c.FormValue("id")
	nama := c.FormValue("nama")
	deskripsi := c.FormValue("deskripsi")

	conv_id, err := strconv.Atoi(id)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	result, err := models.UpdateData(conv_id, nama, deskripsi)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	return c.JSON(http.StatusOK, result)

}

func DeleteController(c echo.Context) error {
	id := c.Param("id")

	conv_id, err := strconv.Atoi(id)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	result, err := models.DeletData(conv_id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	return c.JSON(http.StatusOK, result)

}
